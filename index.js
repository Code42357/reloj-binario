window.onload = function () {
  setInterval(loop, 1000);
  var canvas = document.getElementById("canvas");
  Array(3)
    .fill(0)
    .map(() => document.createElement("div"))
    .forEach((tiempo, i) => {
      tiempo.className = i === 0 ? "Horas" : i == 1 ? "Minutos" : "Segundos";
      Array(6)
        .fill(0)
        .map(() => document.createElement("div"))
        .forEach((x) => tiempo.appendChild(x));
      canvas.appendChild(tiempo);
    });
};

function setColor(numero, elementos) {
  var i = 0;
  while (numero > 0 && i < elementos.length) {
    elementos[i++].className = numero & 1 ? "digito" : "";
    numero = numero / 2;
  }
}

function loop() {
  var ahora = new Date();
  document.getElementById("title").innerHTML =
    "RELOJ BINARIO " +
    (ahora.getUTCHours() <= 9 ? "0" : "") +
    ahora.getUTCHours() +
    ":" +
    (ahora.getUTCMinutes() <= 9 ? "0" : "") +
    ahora.getUTCMinutes() +
    ":" +
    (ahora.getUTCSeconds() <= 9 ? "0" : "") +
    ahora.getUTCSeconds();
  setColor(
    ahora.getUTCHours(),
    document.getElementsByClassName("Horas")[0].childNodes
  );
  setColor(
    ahora.getUTCMinutes(),
    document.getElementsByClassName("Minutos")[0].childNodes
  );
  setColor(
    ahora.getUTCSeconds(),
    document.getElementsByClassName("Segundos")[0].childNodes
  );
}
